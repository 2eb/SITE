---
layout:
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Weekly template

{% hint style="info" %}
Duplicate this page for every day you want to track.
{% endhint %}

## Today's stuff

<table data-header-hidden><thead><tr><th width="80" data-type="checkbox"></th><th width="120" data-type="select">Type</th><th>Action</th></tr></thead><tbody><tr><td>false</td><td></td><td>A meeting I'm attending</td></tr><tr><td>false</td><td></td><td>A task I should do</td></tr></tbody></table>

{% tabs %}
{% tab title="Monday" %}
<table><thead><tr><th>Item</th><th>Notes</th><th data-type="select">Status</th></tr></thead><tbody><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr></tbody></table>
{% endtab %}

{% tab title="Tuesday" %}

{% endtab %}

{% tab title="Wednesday" %}

{% endtab %}

{% tab title="Thursday" %}

{% endtab %}

{% tab title="Untitled" %}

{% endtab %}
{% endtabs %}

## Notes & ideas

<details>

<summary>Notes: A meeting I’m attending</summary>

Start taking notes…

</details>

## How was the day?

<details>

<summary>🧠 Mood tracking</summary>

Start taking notes…

</details>

<details>

<summary>💡 Observations</summary>

Start taking notes…

</details>

{% hint style="info" %}
**GitBook tip:** Use the **rating** column in a table to build a super simple habit-tracking section.
{% endhint %}

<table data-header-hidden><thead><tr><th width="120" data-type="rating" data-max="5"></th><th>Task</th></tr></thead><tbody><tr><td>4</td><td>Sleep</td></tr><tr><td>3</td><td>Work/life balance</td></tr><tr><td>3</td><td>Creativity</td></tr><tr><td>5</td><td>Fitness</td></tr></tbody></table>
