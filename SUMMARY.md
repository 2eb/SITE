# Table of contents

* [Weekly template](README.md)

## This week

* [Dec. 4-8, 2023](this-week/dec.-4-8-2023.md)

## Archive

* [2022](archive/2022/README.md)
  * [July](archive/2022/july/README.md)
    * [1 Jul - Fri](archive/2022/july/1-jul-fri.md)
