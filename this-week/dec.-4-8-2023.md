---
layout: landing
---

# Dec. 4-8, 2023

## This Week's Tasks

<table><thead><tr><th data-type="number"></th><th>Action</th><th data-type="select">Type</th><th data-type="select">Status</th></tr></thead><tbody><tr><td>0</td><td>😇 1:1 with Gloria</td><td></td><td></td></tr><tr><td>0</td><td>🧠 Brainstorm brand ideas</td><td></td><td></td></tr><tr><td>0</td><td>👩🏽‍💻 Code up new homepage</td><td></td><td></td></tr><tr><td>0</td><td>🐶 Walk the pooch</td><td></td><td></td></tr></tbody></table>

## Notes & ideas

<details>

<summary></summary>



</details>

## How was the day?

<details>

<summary>🧠 Mood tracking</summary>

Start taking notes…

</details>

<details>

<summary>💡 Observations</summary>

Start taking notes…

</details>

{% hint style="info" %}
**GitBook tip:** Use the **rating** column in a table to build a super simple habit-tracking section.
{% endhint %}

<table data-header-hidden><thead><tr><th width="120" data-type="rating" data-max="5"></th><th>Task</th></tr></thead><tbody><tr><td>4</td><td>Sleep</td></tr><tr><td>3</td><td>Work/life balance</td></tr><tr><td>3</td><td>Creativity</td></tr><tr><td>5</td><td>Fitness</td></tr></tbody></table>
